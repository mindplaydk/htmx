"use strict";
var htmx;
(function (htmx) {
    function parse(literals) {
        var placeholders = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            placeholders[_i - 1] = arguments[_i];
        }
        var inputs = [];
        for (var i = 0; i < placeholders.length; i++) {
            inputs.push(literals[i]);
            inputs.push(placeholders[i]);
        }
        inputs.push(literals[literals.length - 1]);
        var root = { type: "", attrs: [], children: [] };
        build(root, inputs);
        return root.children;
    }
    htmx.parse = parse;
    function mount(target) {
    }
    htmx.mount = mount;
    function render(nodes, parent) {
        for (var _i = 0, nodes_1 = nodes; _i < nodes_1.length; _i++) {
            var node = nodes_1[_i];
            if (node.hasOwnProperty("type")) {
                var v_el = node;
                var el = document.createElement(v_el.type);
                parent.appendChild(el);
                var _loop_1 = function (name_1) {
                    var value = v_el.attrs[name_1];
                    var type = typeof value;
                    switch (type) {
                        case "string":
                            el.setAttribute(name_1, value);
                            break;
                        case "number":
                            el.setAttribute(name_1, "" + value);
                            break;
                        case "object":
                            if (name_1 === "class") {
                                el.setAttribute("class", Object.keys(value).filter(function (n) { return value[n]; }).join(" "));
                                break;
                            }
                            if (name_1 === "style") {
                                for (var prop in value) {
                                    el.style[prop] = value[prop];
                                }
                                break;
                            }
                            throw new Error("unexpected object interpolation");
                        case "function":
                            if (name_1.indexOf("on") !== 0) {
                                throw new Error("unexpected function interpolation for attribute " + name_1);
                            }
                            var event_name = name_1.substr(2);
                            el.addEventListener(event_name, value);
                            break;
                        default:
                            throw new Error("unsupported interpolated type: " + type);
                    }
                };
                for (var name_1 in v_el.attrs) {
                    _loop_1(name_1);
                }
                render(v_el.children, el);
            }
            else {
                parent.appendChild(document.createTextNode(node.text));
            }
        }
    }
    htmx.render = render;
    function escape(str) {
        return str.replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    }
    var SELF_CLOSING = "AREA|BASE|BR|COL|COMMAND|EMBED|HR|IMG|INPUT|KEYGEN|LINK|META|PARAM|SOURCE|TRACK|WBR".split("|");
    var WHITESPACE = /^\s*/;
    var NAME = /^[a-zA-Z_][a-zA-Z0-9\-_\.]+/;
    var READY = 0;
    var START_TAG = 1;
    var TEXT = 2;
    var END_TAG = 3;
    var IN_TAG = 4;
    var ATTR_NAME = 5;
    var ATTR_VALUE = 6;
    function build(root, inputs) {
        var state = READY;
        var target = root;
        var stack = [];
        var attr_name = "";
        var el_name = "";
        var self_closing = false;
        var offset = 0;
        var total = 0;
        var attrs = {};
        function emit() {
            target.children.push({
                type: el_name,
                attrs: attrs,
                children: []
            });
            attrs = {};
            if (!self_closing) {
                target = target.children[target.children.length - 1];
                stack.push(target);
            }
        }
        function error(message) {
            return new Error(message + " at: " + (offset + total));
        }
        function parse(input) {
            total += offset;
            offset = 0;
            function peek() {
                return input.charAt(offset);
            }
            function consume(pattern, optional) {
                if (optional === void 0) { optional = false; }
                var match = pattern.exec(input.substr(offset));
                if (match) {
                    offset += match[0].length;
                    return match[0];
                }
                if (optional) {
                    return "";
                }
                throw error("unexpected input");
            }
            while (offset < input.length) {
                switch (state) {
                    case READY:
                        if (consume(WHITESPACE)) {
                            continue;
                        }
                        if (peek() === "<") {
                            offset += 1;
                            state = START_TAG;
                            break;
                        }
                        state = TEXT;
                        break;
                    case START_TAG:
                        if (peek() === "/") {
                            offset += 1;
                            state = END_TAG;
                            break;
                        }
                        el_name = consume(NAME).toUpperCase();
                        self_closing = SELF_CLOSING.indexOf(el_name) !== -1;
                        state = IN_TAG;
                        break;
                    case IN_TAG:
                        if (consume(WHITESPACE)) {
                            continue;
                        }
                        if (peek() === ">") {
                            offset += 1;
                            state = READY;
                            emit();
                            break;
                        }
                        state = ATTR_NAME;
                        break;
                    case END_TAG:
                        var name_2 = consume(NAME).toUpperCase();
                        if (target.type !== name_2) {
                            throw error("unexpected end tag: " + name_2 + ", expected: " + target.type);
                        }
                        consume(WHITESPACE);
                        if (peek() !== ">") {
                            throw error("unexpected input - expected closing tag");
                        }
                        offset += 1;
                        state = READY;
                        stack.pop();
                        target = stack[stack.length - 1];
                        break;
                    case ATTR_NAME:
                        attr_name = consume(NAME).toLowerCase();
                        consume(WHITESPACE);
                        if (peek() === "=") {
                            offset += 1;
                            state = ATTR_VALUE;
                        }
                        else {
                            attrs[attr_name] = "";
                            state = IN_TAG;
                        }
                        break;
                    case ATTR_VALUE:
                        if (peek() === '"') {
                            offset += 1;
                            var attr_value = consume(/^[^\"]*/);
                            if (peek() !== '"') {
                                throw error("missing ending double quote");
                            }
                            offset += 1;
                            attrs[attr_name] = attr_value;
                            state = IN_TAG;
                        }
                        break;
                    case TEXT:
                        var text = consume(/^[^\<]*/);
                        if (text) {
                            target.children.push({ text: text });
                        }
                        state = READY;
                        break;
                    default:
                        throw error("internal error - unknown state: " + state);
                }
            }
        }
        for (var _i = 0, inputs_1 = inputs; _i < inputs_1.length; _i++) {
            var input = inputs_1[_i];
            var type = typeof input;
            switch (type) {
                case "string":
                    parse(input);
                    break;
                case "number":
                    parse(input.toString());
                    break;
                case "object":
                    if (state === ATTR_VALUE) {
                        attrs[attr_name] = input;
                        state = IN_TAG;
                        break;
                    }
                    if (state === IN_TAG) {
                        for (var prop in input) {
                            attrs[prop] = input[prop];
                        }
                        break;
                    }
                    throw error("unexpected object interpolation");
                case "function":
                    if (state === ATTR_VALUE) {
                        attrs[attr_name] = input;
                        state = IN_TAG;
                        break;
                    }
                    throw error("unexpected function interpolation");
                default:
                    throw error("unsupported interpolated type: " + type);
            }
        }
    }
})(htmx || (htmx = {}));
var render = htmx.mount(document.body);
var message = "Hello World";
function hello() {
    console.log("hello");
}
var v = (_a = ["<div id=\"foo\"><label><input ", " onclick=", " checked>I said: <span style=", " class=", ">", "</span></label></div>"], _a.raw = ["<div id=\"foo\"><label><input ", " onclick=", " checked>I said: <span style=", " class=", ">", "</span></label></div>"], htmx.parse(_a, { type: "checkbox" }, hello, { fontSize: "50px" }, { red: true, blue: false }, message));
htmx.render(v, document.body);
console.log(v);
var _a;
