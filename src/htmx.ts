module htmx {
    export interface AttrMap {
        [name: string]: any;
    }

    export interface VText {
        text: string;
    }

    export interface VElement {
        type: string;
        attrs: AttrMap;
        children: VNode[];
    }

    export type VNode = VElement | VText;

    export function parse(literals: TemplateStringsArray, ...placeholders: any[]): VNode[] {
        let inputs = [];

        for (let i = 0; i < placeholders.length; i++) {
            inputs.push(literals[i]);
            inputs.push(placeholders[i]);
        }

        inputs.push(literals[literals.length - 1]);

        const root: VElement = { type: "", attrs: [], children: [] };

        build(root, inputs);

        return root.children;
    }

    export function mount(target: HTMLElement) {
    }

    export function render(nodes: VNode[], parent: HTMLElement) {
        for (let node of nodes) {
            if (node.hasOwnProperty("type")) {
                const v_el = node as VElement;
                const el = document.createElement(v_el.type);

                parent.appendChild(el);

                for (const name in v_el.attrs) {
                    const value = v_el.attrs[name];
                    const type = typeof value;
        
                    switch (type) {
                        case "string":
                        el.setAttribute(name, value);
        
                            break;
        
                        case "number":
                        el.setAttribute(name, "" + value);
        
                            break;
        
                        case "object":
                            if (name === "class") {
                                el.setAttribute("class", Object.keys(value).filter(n => value[n]).join(" "));
        
                                break;
                            }
        
                            if (name === "style") {
                                for (let prop in value) {
                                    el.style[<any>prop] = value[prop];
                                }
        
                                break;
                            }
        
                            throw new Error("unexpected object interpolation");
        
                        case "function":
                            if (name.indexOf("on") !== 0) {
                                throw new Error(`unexpected function interpolation for attribute ${name}`);
                            }
        
                            const event_name = name.substr(2);
        
                            el.addEventListener(event_name, value);
        
                            break;
        
                        default:
                            throw new Error(`unsupported interpolated type: ${type}`);
                    }
                }

                render(v_el.children, el);
            } else {
                parent.appendChild(document.createTextNode((node as VText).text));
            }
        }
    }

    function escape(str: string): string {
        return str.replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    }

    const SELF_CLOSING = "AREA|BASE|BR|COL|COMMAND|EMBED|HR|IMG|INPUT|KEYGEN|LINK|META|PARAM|SOURCE|TRACK|WBR".split("|");

    const WHITESPACE = /^\s*/;
    const NAME = /^[a-zA-Z_][a-zA-Z0-9\-_\.]+/;

    const READY = 0;
    const START_TAG = 1;
    const TEXT = 2;
    const END_TAG = 3;
    const IN_TAG = 4;
    const ATTR_NAME = 5;
    const ATTR_VALUE = 6;

    function build(root: VElement, inputs: any[]) {
        let state = READY;
        let target: VElement = root;
        let stack: VElement[] = [];
        let attr_name = "";
        let el_name = "";
        let self_closing = false;
        let offset = 0;
        let total = 0;
        let attrs: AttrMap = {};

        function emit() {
            target.children.push({
                type: el_name,
                attrs: attrs,
                children: []
            });

            attrs = {};

            if (! self_closing) {
                target = target.children[target.children.length - 1] as VElement;
    
                stack.push(target);
            }
        }

        function error(message: string) {
            return new Error(`${message} at: ${offset + total}`);
        }

        function parse(input: string) {
            total += offset;
            offset = 0;

            function peek(): string {
                return input.charAt(offset);
            }

            function consume(pattern: RegExp, optional = false): string {
                const match = pattern.exec(input.substr(offset));

                if (match) {
                    offset += match[0].length;

                    return match[0];
                }

                if (optional) {
                    return "";
                }

                throw error("unexpected input");
            }

            while (offset < input.length) {
                switch (state) {
                    case READY:
                        if (consume(WHITESPACE)) {
                            continue;
                        }

                        if (peek() === "<") {
                            offset += 1;
                            state = START_TAG;
                            break;
                        }

                        state = TEXT;

                        break;

                    case START_TAG:
                        if (peek() === "/") {
                            offset += 1;
                            state = END_TAG;
                            break;
                        }

                        el_name = consume(NAME).toUpperCase();

                        self_closing = SELF_CLOSING.indexOf(el_name) !== -1;

                        state = IN_TAG;

                        break;

                    case IN_TAG:
                        if (consume(WHITESPACE)) {
                            continue;
                        }

                        if (peek() === ">") {
                            offset += 1;
                            state = READY;

                            emit();

                            break;
                        }

                        state = ATTR_NAME;

                        break;

                    case END_TAG:
                        const name = consume(NAME).toUpperCase();

                        if (target.type !== name) {
                            throw error(`unexpected end tag: ${name}, expected: ${target.type}`);
                        }

                        consume(WHITESPACE);

                        if (peek() !== ">") {
                            throw error(`unexpected input - expected closing tag`);
                        }

                        offset += 1;

                        state = READY;

                        stack.pop();

                        target = stack[stack.length - 1];

                        break;

                    case ATTR_NAME:
                        attr_name = consume(NAME).toLowerCase();

                        consume(WHITESPACE);

                        if (peek() === "=") {
                            offset += 1;
                            state = ATTR_VALUE;
                        } else {
                            attrs[attr_name] = "";
                            state = IN_TAG;
                        }

                        break;

                    case ATTR_VALUE:
                        if (peek() === '"') {
                            offset += 1;

                            let attr_value = consume(/^[^\"]*/);

                            if (peek() !== '"') {
                                throw error("missing ending double quote");
                            }

                            offset += 1;

                            attrs[attr_name] = attr_value;

                            state = IN_TAG;
                        }

                        break;

                    case TEXT:
                        let text = consume(/^[^\<]*/);

                        if (text) {
                            target.children.push({ text });
                        }

                        state = READY;

                        break;

                    default:
                        throw error("internal error - unknown state: " + state);
                }
            }
        }

        for (let input of inputs) {
            let type = typeof input;

            switch (type) {
                case "string":
                    parse(input);

                    break;

                case "number":
                    parse(input.toString());

                    break;

                case "object":
                    if (state === ATTR_VALUE) {
                        attrs[attr_name] = input;

                        state = IN_TAG;

                        break;
                    }

                    if (state === IN_TAG) {
                        for (let prop in input) {
                            attrs[prop] = input[prop];
                        }

                        break;
                    }

                    throw error("unexpected object interpolation");

                case "function":
                    if (state === ATTR_VALUE) {
                        attrs[attr_name] = input;

                        state = IN_TAG;

                        break;
                    }

                    throw error("unexpected function interpolation");

                default:
                    throw error(`unsupported interpolated type: ${type}`);
            }
        }
    }
}

const render = htmx.mount(document.body);

const message = "Hello World";

function hello() {
    console.log("hello");
}

let v = htmx.parse`<div id="foo"><label><input ${{ type: "checkbox" }} onclick=${ hello } checked>I said: <span style=${{ fontSize:"50px" }} class=${{ red: true, blue: false }}>${ message }</span></label></div>`;

htmx.render(v, document.body);

console.log(v);

// const items = ["one", "two"];

// htmx.parse`<ul>${ items.map(i => `<li>${i}</li>`) }</ul>`;
